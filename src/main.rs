#![feature(with_options)] //File::with_options() is unstable stuff :(

use anyhow::{Result, anyhow, Context};

use ifstructs::ifreq;

use std::os::unix::io::{AsRawFd, RawFd};
use std::fs::File;
use std::path::Path;

//referenced C kernel->userland interface given at
//https://www.kernel.org/doc/html/latest/networking/tuntap.html


//TODO TODO maybe there is a nice abstraction in nix crate
//or sth so we don't have to use raw ints
struct TunTapDevice {
    fd: RawFd,
    devtype: TunTapDeviceType,
    ifname: String,
}

//referenced for C-style enum
//https://elixir.bootlin.com/linux/latest/source/include/uapi/linux/if_tun.h#L65
enum IoctlFlags {
    //TODO find out if specifiying type needed even
    #[allow(non_camel_case_types)]
    IFF_TUN     = 0x0001,
    #[allow(non_camel_case_types)]
    IFF_TAP     = 0x0002,
    #[allow(non_camel_case_types)]
    IFF_NO_PI   = 0x1000,
}

enum TunTapDeviceType {
    Tun,
    Tap,
    //TODO 3rd type
}

fn mkdev(altpath: Option<&Path>, ifname: Option<String>,
            devtype: TunTapDeviceType) -> Result<()> {
    //if for soem fucked up reason the tun devices is to be found some
    //where else
    let tundev_path = altpath.unwrap_or(Path::new("/dev/net/tun"));
    let fd = File::with_options().read(true).write(true)
        .open(tundev_path).context(
            format!("couldn't get a file descriptor on \"{}\"",
                tundev_path.to_string_lossy()))?
        .as_raw_fd();

    //TODO checkup if its also possible to pass nothing
    //and have the kernel decide or something
    let device = TunTapDevice {
        fd: fd,
        ifname: ifname.unwrap_or( match devtype {
            TunTapDeviceType::Tap => "tap%d".to_string(),
            TunTapDeviceType::Tun => "tun%d".to_string(),
        }),
        devtype: devtype,
    };


    let ifr = ifreq::from_name(&device.ifname)?
        .set_flags( match &device.devtype {
            TunTapDeviceType::Tun => IoctlFlags::IFF_TAP,
            TunTapDeviceType::Tap => IoctlFlags::IFF_TUN,
            //TunTapDeviceType::XYZ=> IoctlFlags::IFF_TAP,
        } as i16 ); //TODO can the as i16 part be done prettier?

    Ok(())
}


fn main() -> Result<()> {
    //mkdev::<TapDevice>(None)?;
    mkdev(None, Some("rtap99".to_string()), TunTapDeviceType::Tap)?;
    Ok(())
}
